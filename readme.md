# Vivalerts stack of containers

## Deployment steps

 1. Clone this repository and change directory

 2. Create a `.env` file and define there the variables of
    [docker-compose.yml](docker-compose.yml) that you have to define

 3. You may have to `docker login registry.gitlab.com`

 4. `docker compose pull`

 5. `docker compose up --detach`

Note that all the `docker` commands most probably need `sudo`.

Also note that the same commands are used for refreshing the Stack, when
new Docker images have been created.

## Differentiations between production and testing environments

It's unavoidable to have differences between production and testing
setups. Our goal is to make them minimal.

There's also a `TARGET_HOST` environment variable differentiation. This
variable is only used by the Reverse Proxy service. Only if the
`TARGET_HOST` is `vivalerts.com`, the Reverse Proxy service issues or
updates an SSL certificate for the site via
[certificate.sh](https://gitlab.com/vivalerts/reverse-proxy/-/blob/main/certificate.sh).

> ⚠️ __Caution__
> 
> Except for the above two, there're no other differences between
> production and testing environments. For example, Nginx
> [configuration](https://gitlab.com/vivalerts/reverse-proxy/-/blob/main/default.conf)
> is the same for the two. This has the side effect that if you type
> `testing.vivalerts.com` in your browser, it may prefix it by
> `http://`. Note that in newer Chrome versions, they have changed the
> default prefix to `https://`.
> 
> Unfortunately, current Nginx configuration does not (manage to)
> differentiate between `http://testing.vivalerts.com` and
> `http://vivalerts.com` and redirects both to https://vivalerts.com.
> Therefore, please take care to write explicitly the
> <code>http<b>s</b></code> prefix in https://testing.vivalerts.com in
> order not to be redirected to production.
