#!/bin/sh -e

if [ "$TARGET_HOST" = "vivalerts.com" ]; then

  if [ "$TAG_CRAWLER" != "" ]; then
    TAG_CRAWLER=":$TAG_CRAWLER"
  fi

  curl -fsSH "PRIVATE-TOKEN: $SNIPPET_TOKEN" https://gitlab.com/api/v4/snippets/3695420/raw |
  grep -v "^Test," |
  while read -r LINE; do
    ACCOUNT_TYPE=$(echo "$LINE" | cut -d, -f1)
    COUNTRY=$(echo "$LINE" | cut -d, -f2)
    LOCATION_ID=$(echo "$LINE" | cut -d, -f3 | sed "s/ /-/g")
    LOCATION=$(echo "$LINE" | cut -d, -f4)
    LANGUAGE=$(echo "$LINE" | cut -d, -f5)
    AUTH_TOKEN=$(echo "$LINE" | cut -d, -f7)
    CT0=$(echo "$LINE" | cut -d, -f8)

    echo "
  crawl-$LOCATION_ID:
    image: registry.gitlab.com/vivalerts/crawler$TAG_CRAWLER
    restart: unless-stopped
    environment:
      COUNTRY: $COUNTRY
      LOCATION_ID: $LOCATION_ID
      LOCATION: $LOCATION
      LANGUAGE: $LANGUAGE
      ACCOUNT_TYPE: $ACCOUNT_TYPE
      AUTH_TOKEN: $AUTH_TOKEN
      CT0: $CT0
    logging:
      options:
        max-size: 10m" >> docker-compose.yml
  done

fi
